from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from .models import Post
from .forms import ContactForm,BlogForm
from django.core.mail import EmailMessage

# Create your views here.

# def hello_world(request):
# 	return HttpResponse("Hello!! world \n")

## python way of dealing with a template.

# def hello_world(request):
# 	f = open('/home/khyaathi/Documents/bit-tuxfux/django-projects/Batch-206/src/blog/templates/test.html')
# 	content = f.read()
# 	return HttpResponse(content)

def hello_world(request):
	context = {}
	return render(request,'blog/test.html',context)

def home(request):
	blog_data = Post.objects.all()
	context = {'namesdb':blog_data}
	return render(request,'home.html',context)

# day3
# def myblog(request):
# 	context = {'name1':'maneesh','name2':'narendra','name3':'uday',
# 			    'subject':'django','subject1':'',
# 			    'loc1':'nagpur','loc2':'hyderabad','loc3':'bang'}
# 	return render(request,'blog/myblog.html',context)

# day4
# def myblog(request):
# 	context = {'namesdb':[{'name':'maneesh','subject':'','loc':'nagpur'},
# 			    {'name':'narendra','subject':'','loc':'hyderabad'},
# 			    {'name':'uday','subject':'','loc':'banglore'}]}
# 	return render(request,'blog/myblog.html',context)

# day 6
def myblog(request):
	blog_data = Post.objects.all()
	context = {'namesdb':blog_data}
	return render(request,'blog/myblog.html',context)

# day 8
def ContactFormView(request):

	# GET
	contact_form = ContactForm # class not an instance.
	context = {'form':contact_form}
	
	# POST
	if request.method == 'POST':
		contact_form = ContactForm(request.POST)
	# POST AND VALIDITY
		print request.method,contact_form.is_valid(),contact_form
		if contact_form.is_valid():
			contact_name = contact_form.cleaned_data['contact_name']
			contact_email = contact_form.cleaned_data['contact_email']
			content = contact_form.cleaned_data['content']
			subject = "A new contact or lead - {}".format(contact_name)
			email = EmailMessage(subject,contact_name + '\n' + contact_email + '\n' + content, to=['tuxfux.hlp@gmail.com'])
			email.send()
			return HttpResponseRedirect('/blog/thankyou/')
	## POST AND NOT VALID DATA
		else:
			context = {'form':contact_form}

	# GET OR POST
	return render(request,'blog/ContactForm.html',context)


def Thanks(request):
	return HttpResponse("Thank you for contacting us !!!")


def BlogFormView(request):
	# POST
	if request.method == 'POST':
		form = BlogForm(request.POST)
	# VALID
		if form.is_valid():
			author = form.cleaned_data['author']
			email = form.cleaned_data['email']
			title = form.cleaned_data['title']
			text = form.cleaned_data['text']
			created_date = form.cleaned_data['created_date']
			published_date = form.cleaned_data['published_date']
			Post.objects.create(author=author,email=email,title=title,text=text,created_date=created_date,published_date=published_date)
			return HttpResponseRedirect('/blog/thankyou/')
	# NOT VALID	
		else:
			context = {'form':form}
			return render(request,'blog/BlogForm.html',context)
	# GET
	else:
			form = BlogForm
			context = {'form':form}
			return render(request,'blog/BlogForm.html',context)