from django.conf.urls import include, url
from .views import ContactFormView,BlogFormView
from .views import Thanks

urlpatterns = [
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^hello/','blog.views.hello_world',name='hello_world'),
    #url(r'^$','blog.views.myblog',name='myblog'),
    url(r'^$','blog.views.home',name='home'),
    url(r'^contact/',ContactFormView,name='contact'),
    url(r'^Post/',BlogFormView,name='blog'),
    url(r'^thankyou/',Thanks,name='thanks'),
]