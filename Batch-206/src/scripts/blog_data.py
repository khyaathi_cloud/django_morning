#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been automatically generated.
# Instead of changing it, create a file called import_helper.py
# and put there a class called ImportHelper(object) in it.
#
# This class will be specially casted so that instead of extending object,
# it will actually extend the class BasicImportHelper()
#
# That means you just have to overload the methods you want to
# change, leaving the other ones inteact.
#
# Something that you might want to do is use transactions, for example.
#
# Also, don't forget to add the necessary Django imports.
#
# This file was generated with the following command:
# manage.py dumpscript blog
#
# to restore it, run
# manage.py runscript module_name.this_script_name
#
# example: if manage.py is at ./manage.py
# and the script is at ./some_folder/some_script.py
# you must make sure ./some_folder/__init__.py exists
# and run  ./manage.py runscript some_folder.some_script
import os, sys
from django.db import transaction
from django.utils import timezone

class BasicImportHelper(object):

    def pre_import(self):
        pass

    @transaction.atomic
    def run_import(self, import_data):
        import_data()

    def post_import(self):
        pass

    def locate_similar(self, current_object, search_data):
        # You will probably want to call this method from save_or_locate()
        # Example:
        #   new_obj = self.locate_similar(the_obj, {"national_id": the_obj.national_id } )

        the_obj = current_object.__class__.objects.get(**search_data)
        return the_obj

    def locate_object(self, original_class, original_pk_name, the_class, pk_name, pk_value, obj_content):
        # You may change this function to do specific lookup for specific objects
        #
        # original_class class of the django orm's object that needs to be located
        # original_pk_name the primary key of original_class
        # the_class      parent class of original_class which contains obj_content
        # pk_name        the primary key of original_class
        # pk_value       value of the primary_key
        # obj_content    content of the object which was not exported.
        #
        # You should use obj_content to locate the object on the target db
        #
        # An example where original_class and the_class are different is
        # when original_class is Farmer and the_class is Person. The table
        # may refer to a Farmer but you will actually need to locate Person
        # in order to instantiate that Farmer
        #
        # Example:
        #   if the_class == SurveyResultFormat or the_class == SurveyType or the_class == SurveyState:
        #       pk_name="name"
        #       pk_value=obj_content[pk_name]
        #   if the_class == StaffGroup:
        #       pk_value=8

        search_data = { pk_name: pk_value }
        the_obj = the_class.objects.get(**search_data)
        #print(the_obj)
        return the_obj


    def save_or_locate(self, the_obj):
        # Change this if you want to locate the object in the database
        try:
            the_obj.save()
        except:
            print("---------------")
            print("Error saving the following object:")
            print(the_obj.__class__)
            print(" ")
            print(the_obj.__dict__)
            print(" ")
            print(the_obj)
            print(" ")
            print("---------------")

            raise
        return the_obj


importer = None
try:
    import import_helper
    # We need this so ImportHelper can extend BasicImportHelper, although import_helper.py
    # has no knowlodge of this class
    importer = type("DynamicImportHelper", (import_helper.ImportHelper, BasicImportHelper ) , {} )()
except ImportError as e:
    # From Python 3.3 we can check e.name - string match is for backward compatibility.
    if 'import_helper' in str(e):
        importer = BasicImportHelper()
    else:
        raise

import datetime
from decimal import Decimal
from django.contrib.contenttypes.models import ContentType

try:
    import dateutil.parser
except ImportError:
    print("Please install python-dateutil")
    sys.exit(os.EX_USAGE)

def run():
    importer.pre_import()
    importer.run_import(import_data)
    importer.post_import()

def import_data():
    # Initial Imports
    from django.contrib.auth.models import User

    # Processing model: blog.models.Post

    from blog.models import Post

    blog_post_1 = Post()
    blog_post_1.author =  importer.locate_object(User, "id", User, "id", 2, {'username': u'nagendra', 'first_name': u'', 'last_name': u'', 'is_active': True, 'id': 2, 'is_superuser': False, 'is_staff': False, 'last_login': datetime.datetime(2018, 2, 28, 2, 18, 17, 539652, tzinfo=timezone.get_default_timezone()), 'password': u'pbkdf2_sha256$20000$kGVh32bYiqvB$m7IKvxIE4BsJAGIQVZRAGWqaon7bvpYPic8toSSdtpk=', 'email': u'', 'date_joined': datetime.datetime(2018, 2, 17, 1, 59, 30, 680121, tzinfo=timezone.get_default_timezone())} ) 
    blog_post_1.email = None
    blog_post_1.title = u'my first  post - Nagendra'
    blog_post_1.text = u'This is my first post on django admin portal.'
    blog_post_1.created_date = dateutil.parser.parse("2018-02-17T02:00:22+00:00")
    blog_post_1.published_date = None
    blog_post_1 = importer.save_or_locate(blog_post_1)

    blog_post_2 = Post()
    blog_post_2.author =  importer.locate_object(User, "id", User, "id", 4, {'username': u'maneesh', 'first_name': u'', 'last_name': u'', 'is_active': True, 'id': 4, 'is_superuser': False, 'is_staff': False, 'last_login': None, 'password': u'pbkdf2_sha256$20000$CAnL1NygijDJ$M+MojKj60yx25iMZdFp7HZWjS1EDagGH260EcVABk7o=', 'email': u'', 'date_joined': datetime.datetime(2018, 2, 17, 1, 59, 50, tzinfo=timezone.get_default_timezone())} ) 
    blog_post_2.email = None
    blog_post_2.title = u'My first post - Maneesh'
    blog_post_2.text = u'This is the first post by Maneesh.'
    blog_post_2.created_date = dateutil.parser.parse("2018-02-17T02:00:46+00:00")
    blog_post_2.published_date = None
    blog_post_2 = importer.save_or_locate(blog_post_2)

    blog_post_3 = Post()
    blog_post_3.author =  importer.locate_object(User, "id", User, "id", 3, {'username': u'uday', 'first_name': u'', 'last_name': u'', 'is_active': True, 'id': 3, 'is_superuser': False, 'is_staff': False, 'last_login': None, 'password': u'pbkdf2_sha256$20000$lm73WY4uYpCl$k9o8zBNRQNywVMPbBSn9duU8xWPfDI6WAzNqByPBQV4=', 'email': u'', 'date_joined': datetime.datetime(2018, 2, 17, 1, 59, 38, 939154, tzinfo=timezone.get_default_timezone())} ) 
    blog_post_3.email = None
    blog_post_3.title = u'my first post - uday'
    blog_post_3.text = u'Hey there this is my first post on jinja tempaltes.'
    blog_post_3.created_date = dateutil.parser.parse("2018-02-17T02:01:09+00:00")
    blog_post_3.published_date = None
    blog_post_3 = importer.save_or_locate(blog_post_3)

    blog_post_4 = Post()
    blog_post_4.author =  importer.locate_object(User, "id", User, "id", 2, {'username': u'nagendra', 'first_name': u'', 'last_name': u'', 'is_active': True, 'id': 2, 'is_superuser': False, 'is_staff': False, 'last_login': datetime.datetime(2018, 2, 28, 2, 18, 17, 539652, tzinfo=timezone.get_default_timezone()), 'password': u'pbkdf2_sha256$20000$kGVh32bYiqvB$m7IKvxIE4BsJAGIQVZRAGWqaon7bvpYPic8toSSdtpk=', 'email': u'', 'date_joined': datetime.datetime(2018, 2, 17, 1, 59, 30, 680121, tzinfo=timezone.get_default_timezone())} ) 
    blog_post_4.email = None
    blog_post_4.title = u'This is my second post'
    blog_post_4.text = u'Hey i am trying to enter some data using the django modular forms.'
    blog_post_4.created_date = dateutil.parser.parse("2018-02-21T02:32:35+00:00")
    blog_post_4.published_date = None
    blog_post_4 = importer.save_or_locate(blog_post_4)

    blog_post_5 = Post()
    blog_post_5.author =  importer.locate_object(User, "id", User, "id", 2, {'username': u'nagendra', 'first_name': u'', 'last_name': u'', 'is_active': True, 'id': 2, 'is_superuser': False, 'is_staff': False, 'last_login': datetime.datetime(2018, 2, 28, 2, 18, 17, 539652, tzinfo=timezone.get_default_timezone()), 'password': u'pbkdf2_sha256$20000$kGVh32bYiqvB$m7IKvxIE4BsJAGIQVZRAGWqaon7bvpYPic8toSSdtpk=', 'email': u'', 'date_joined': datetime.datetime(2018, 2, 17, 1, 59, 30, 680121, tzinfo=timezone.get_default_timezone())} ) 
    blog_post_5.email = None
    blog_post_5.title = u'This is my second post'
    blog_post_5.text = u'Hey i am trying to enter some data using the django modular forms.'
    blog_post_5.created_date = dateutil.parser.parse("2018-02-21T02:32:35+00:00")
    blog_post_5.published_date = None
    blog_post_5 = importer.save_or_locate(blog_post_5)

    blog_post_6 = Post()
    blog_post_6.author =  importer.locate_object(User, "id", User, "id", 2, {'username': u'nagendra', 'first_name': u'', 'last_name': u'', 'is_active': True, 'id': 2, 'is_superuser': False, 'is_staff': False, 'last_login': datetime.datetime(2018, 2, 28, 2, 18, 17, 539652, tzinfo=timezone.get_default_timezone()), 'password': u'pbkdf2_sha256$20000$kGVh32bYiqvB$m7IKvxIE4BsJAGIQVZRAGWqaon7bvpYPic8toSSdtpk=', 'email': u'', 'date_joined': datetime.datetime(2018, 2, 17, 1, 59, 30, 680121, tzinfo=timezone.get_default_timezone())} ) 
    blog_post_6.email = u'nagendra.django@gmail.com'
    blog_post_6.title = u'my third blog'
    blog_post_6.text = u'Hey there i tried to rectify my email content in the myblog.html stuff. '
    blog_post_6.created_date = dateutil.parser.parse("2018-02-21T02:38:52+00:00")
    blog_post_6.published_date = None
    blog_post_6 = importer.save_or_locate(blog_post_6)

