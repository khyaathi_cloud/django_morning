1. language and framework ?

ex: language : python
ex: framework : django

A language is syntax,grammer and sematics. Developers/implementers are required to support.
A framework is a cohesive set of library code that together simplifies programming in any given language.

- similarity of project.
- integration.
- ease of usage.
- Default behaviour 
ex: client server model -> server pushes the changes to clients.
    client server model -> client pulls the changes from the server.

2. What are various framework in python?
- web frameworks :django,flask,bottle,web2py
- testing frameworks: nose(unittest)
- infrastructure framework: ansible,fabric,paramiko

3. Architecture of django.

MVT - model view and template
MVC - model view and Controller

Reference:
https://www.djangoproject.com/
https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller
https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter
https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93adapter


## Installation of django

khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ virtualenv myenv
New python executable in /home/khyaathi/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/myenv/bin/python
Installing setuptools, pip, wheel...done.
khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ source myenv/bin/activate
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ 
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ pip freeze
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ 
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ pip install django==1.8
Collecting django==1.8
  Using cached Django-1.8-py2.py3-none-any.whl
Installing collected packages: django
Successfully installed django-1.8
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ pip freeze
Django==1.8
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ 

IDE
---

# install pycharm in ubuntu

https://itsfoss.com/install-pycharm-ubuntu/
* Djaneiro -> plugin

# install sublime-text in ubuntu
http://tipsonubuntu.com/2015/03/27/install-sublime-text-2-3-ubuntu-15-04/
* Sublime-jedi

# komodo

### sublime text

* project -> save project as -> batch206.sublime-project
* preference -> colour Schema -> mac classic
* Project -> add folder to project -> add your folder
* view -> side -> show side bar
* view -> show mini map

###
#  lets create a site called mysite
###

(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ django-admin.py startproject mysite
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ pwd
/home/khyaathi/Documents/tuxfux-hlp-notes/Django-notes/Batch-206
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ 

* Add the project in your sublime text -> Add folders to project -> mysite
* Later rename the toplevel mysite to src.

###
# see if your server is running
###

(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ ls
myenv  notes  src
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206$ cd src
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/src$ ls
manage.py  mysite
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/src$ python manage.py runserver
Performing system checks...

System check identified no issues (0 silenced).

You have unapplied migrations; your app may not work properly until they are applied.
Run 'python manage.py migrate' to apply them.

February 10, 2018 - 02:16:26
Django version 1.8, using settings 'mysite.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
[10/Feb/2018 02:17:05]"GET / HTTP/1.1" 200 1767
[10/Feb/2018 02:17:05]"GET /favicon.ico HTTP/1.1" 404 1936


(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/src$ ip a s
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:64:1e:3d brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic enp0s3
       valid_lft 83162sec preferred_lft 83162sec
    inet6 fe80::62ff:cc55:ef85:8430/64 scope link 
       valid_lft forever preferred_lft forever
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/src$ python manage.py runserver 0.0.0.0:8000
Performing system checks...

System check identified no issues (0 silenced).

You have unapplied migrations; your app may not work properly until they are applied.
Run 'python manage.py migrate' to apply them.

February 10, 2018 - 02:20:15
Django version 1.8, using settings 'mysite.settings'
Starting development server at http://0.0.0.0:8000/
Quit the server with CONTROL-C.
[10/Feb/2018 02:20:30]"GET / HTTP/1.1" 200 1767
[10/Feb/2018 02:20:30]"GET /favicon.ico HTTP/1.1" 404 1936
[10/Feb/2018 02:20:30]"GET /favicon.ico HTTP/1.1" 404 1936

###
# to sort the unapplied migrations , please run the migrate command.
###

(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/src$ python manage.py migrate
Operations to perform:
  Synchronize unmigrated apps: staticfiles, messages
  Apply all migrations: admin, contenttypes, auth, sessions
Synchronizing apps without migrations:
  Creating tables...
    Running deferred SQL...
  Installing custom SQL...
Running migrations:
  Rendering model states... DONE
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying sessions.0001_initial... OK
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/src$ 

###
# How to create an admin user
###


(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/src$ python manage.py createsuperuser
Username (leave blank to use 'khyaathi'): admin
Email address: admin@gmail.com
Password: 
Password (again): 
Superuser created successfully.
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/src$ python manage.py runserver 0.0.0.0:8000
Performing system checks...

System check identified no issues (0 silenced).
February 10, 2018 - 02:26:44
Django version 1.8, using settings 'mysite.settings'
Starting development server at http://0.0.0.0:8000/
Quit the server with CONTROL-C.

