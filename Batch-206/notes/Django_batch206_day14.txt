Reference:
https://getbootstrap.com/docs/3.3/css/#forms
https://docs.djangoproject.com/en/2.0/ref/request-response/


a) enable login,logout and register button in you navbar.html page.

<div class="blog-masthead">
<div class="container">
<nav class="blog-nav">
		<a class="blog-nav-item active" href="{% url 'home' %}">Home</a>
		<a class="blog-nav-item" href="{% url 'blog' %}">Enter story</a>
		<a class="blog-nav-item" href="{% url 'contact'  %}">Contact</a>

		{% if request.user.is_authenticated %}
				<a class="blog-nav-item navbar-right" href="{% url 'auth_logout'  %}">{{ request.user }} | Logout</a>
		{% endif %}

		{% if not request.user.is_authenticated and not '/accounts/login' in request.get_full_path %}
			<a class="blog-nav-item navbar-right" href="{% url 'registration_register'  %}">Register</a>
			<!-- <a class="blog-nav-item navbar-right" href="{% url 'auth_login'  %}">Login</a> -->


				<form class="form-inline blog-nav-item navbar-right" action="{% url 'auth_login' %}" method="POST">{%csrf_token%}
						  <div class="form-group">
						   	<input type="text" class="form-control" name="username" placeholder="username">
						  </div>
						  <div class="form-group">
						    <input type="password" class="form-control" name="password" placeholder="password">
						  </div>
						  <button type="submit" class="btn btn-default">Submit</button>
				 </form>


		{% endif %}


</nav>
</div>
</div>