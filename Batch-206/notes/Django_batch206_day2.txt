## migrations

versions prior to 1.8
* there never used to be migrate command.
* python manage.py syncdb
- create yout tables inside the database.
- create the admin user.

https://south.readthedocs.io/en/latest/

###
# create your blog
###

(myenv) khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/django-projects/Batch-206/src$ python manage.py startapp blog
(myenv) khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/django-projects/Batch-206/src$ pwd
/home/khyaathi/Documents/bit-tuxfux/django-projects/Batch-206/src
(myenv) khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/django-projects/Batch-206/src$ ls
blog  db.sqlite3  manage.py  mysite
(myenv) khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/django-projects/Batch-206/src$ 

- migrations folder - for every migration a new file gets created called .py extension.
- admin.py - for managing the admin portal.
- models.py - database schemas.
- tests.py - for testing purpose.
- views.py - view related to the core loging on a particular url.

###
# our first hello world to the world !!!
###

- src/mysite/urls.py

from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$','blog.views.hello_world',name='hello_world'),
]


- src/blog/views.py

from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def hello_world(request):
	return HttpResponse("Hello!! world \n")

- run your server
# python manage.py runserver 0.0.0.0:8000

ex:
http://<publicip>:8000
or
http://127.0.0.1:8000
or
http://localhost:8000


###
# A generic example of reading a template file - html
###

- create a templates folder under your blog app.
- src/blog/templates
- create a test.html file.

 <!DOCTYPE html>
<html>
<head>
<title>My first django page</title>
</head>
<body>

<h1>Hello this is the day 3</h1>
<p>This is a paragraph on django.</p>

</body>
</html> 

reference: https://www.w3schools.com/html/

- src/mysite/urls.py

from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$','blog.views.hello_world',name='hello_world'),
]


- src/blog/views.py

from django.shortcuts import render
from django.http import HttpResponse

def hello_world(request):
	f = open('/home/khyaathi/Documents/bit-tuxfux/django-projects/Batch-206/src/blog/templates/test.html')
	content = f.read()
	return HttpResponse(content)

- run your server 
# python manage.py runserver 0.0.0.0:8000

- http://127.0.0.1:8000


###
# Rendering the templates.
### 

reference: https://docs.djangoproject.com/en/2.0/intro/tutorial03/#a-shortcut-render

- create a templates folder under your blog app.
- src/blog/templates
- create a test.html file.

 <!DOCTYPE html>
<html>
<head>
<title>My first django page</title>
</head>
<body>

<h1>Hello this is the day 3</h1>
<p>This is a paragraph on django.</p>

</body>
</html> 

reference: https://www.w3schools.com/html/

- src/mysite/urls.py

from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$','blog.views.hello_world',name='hello_world'),
]

- src/blog/views.py

The render() function takes the request object as its first argument, a template name as its second argument and a dictionary as its optional third argument.


def hello_world(request):
	context = {}
	return render(request,'test.html',context)

## run your server
# python manage.py runserver

# open the broswer

http://127.0.0.1:8000

Template-loader postmortem

Django tried loading these templates, in this order:
Using loader django.template.loaders.filesystem.Loader:
Using loader django.template.loaders.app_directories.Loader:
/home/khyaathi/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/myenv/local/lib/python2.7/site-packages/django/contrib/admin/templates/test.html (File does not exist)
/home/khyaathi/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/myenv/local/lib/python2.7/site-packages/django/contrib/auth/templates/test.html (File does not exist)


Solution:

- add the 'blog' to the INSTALLED_APPS location of src/mysite/setting.py file.

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'blog',
)

- when ever we add a new app to our setting.py file.
# python manage.py makemigrations
# python manage.py migrate

(myenv) khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/django-projects/Batch-206/src$ python manage.py makemigrations
No changes detected
(myenv) khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/django-projects/Batch-206/src$ python manage.py migrate
Operations to perform:
  Synchronize unmigrated apps: staticfiles, messages
  Apply all migrations: admin, contenttypes, auth, sessions
Synchronizing apps without migrations:
  Creating tables...
    Running deferred SQL...
  Installing custom SQL...
Running migrations:
  No migrations to apply.
(myenv) khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/django-projects/Batch-206/src$ 

# open the broswer

http://127.0.0.1:8000

we should be good to render the template - test.html page.

Postmortem on why it started to work
----------------------------------------

* Django will read the templates folder in our apps.
*  django.template.loaders.app_directories.Loader , will look for apps in the INSTALLED_APPS location and going over 
each app one by one.
* if we have similar kind of templates in two apps, the first app template will be given preference.
* To avoid the clash of templates, we can create a subfolder with name of name of app and dump template into it.

Template-loader postmortem

Django tried loading these templates, in this order:
Using loader django.template.loaders.filesystem.Loader:
Using loader django.template.loaders.app_directories.Loader:
/home/khyaathi/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/myenv/local/lib/python2.7/site-packages/django/contrib/admin/templates/test.html (File does not exist)
/home/khyaathi/Documents/tuxfux-hlp-notes/Django-notes/Batch-206/myenv/local/lib/python2.7/site-packages/django/contrib/auth/templates/test.html (File does not exist)
/home/khyaathi/Documents/bit-tuxfux/django-projects/Batch-206/src/blog/templates/test.html (File does not exist)


