# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('check_app', '0002_auto_20171010_0210'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='reporter',
            field=models.ForeignKey(to='check_app.Reporter', on_delete=django.db.models.deletion.DO_NOTHING),
        ),
    ]
