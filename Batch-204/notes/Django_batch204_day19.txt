# deployment - pythonanywhere

1) making changes to the settings files.
a) A different setting for development.
b) A different setting for production.

1) changes to the setting for the project.

* create a folder called settings in your project folder.
* create a __init__.py file to make sure that the file behaves like a project.
* create a settings/base.py file.
* rename settings.py to sv.settings.py file name.
* copy the settings.py file content to base.py file.
* create settings/dev.py and setings/prod.py.
* we now need to populate __init__.py  with right values. 

from .base import *

try:
	from .dev import *
except:
	pass

# try:
# 	from .prod import *
# except:
# 	pass

i commented out the prod values to make sure that i activate it with prod modification.

2) We will try to make the changes to the BASE_DIR so that we can still point to src.

- Made these modifications in settings/base.py,settings/prod.py,settings/dev.py

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

3) make some modifcation to the allowed hosts for both dev.py and prod.py

- dev.py

DEBUG = True
ALLOWED_HOSTS = ['localhost','127.0.0.1']

- prod.py

# SECURITY WARNING: don't run with debug turned on in production!

DEBUG = False
ALLOWED_HOSTS = ['tuxfux.pythonanywhere.com']


# mysql - pythonanywhere settings.
DATABASES = {
    'default': {
        'ENGINE': 'tuxfux.mysql.pythonanywhere-services.com',
        'NAME': 'tuxfux$myblog1',
        'HOST': '127.0.0.1',  # localhost
        'USER': 'tuxfux',
        'PASSWORD':'redhat123',
    }
}

3) we need to copy the project to our pythonanywhere site.

a) i will take the requirments file from our myenv folder.

(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-204/src$ pip freeze > requirements.txt
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-204/src$ pwd
/home/khyaathi/Documents/tuxfux-hlp-notes/Django-notes/Batch-204/src
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-204/src$ cat requirements.txt 
Django==1.8
django-crispy-forms==1.6.1
django-extensions==1.9.1
django-registration-redux==1.7
MySQL-python==1.2.5
mysqlclient==1.3.12
PyMySQL==0.7.11
python-dateutil==2.6.1
six==1.11.0
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-204/src$ 

b) take a complete tar of your src folder.

(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-204$ ls
myenv  mysite.sublime-project  mysite.sublime-workspace  notes  src
(myenv) khyaathi@khyaathi-Technologies:~/Documents/tuxfux-hlp-notes/Django-notes/Batch-204$ tar -cvf src.tar src

c) Python anywhere modification.

1) upload the src.tar file to the location using the upload button.
2) login to one of the bash consoles and run the below command.
# tar -xvf src.tar

3) Create a virtual env - myenv inside the home directory.
# virtualenv myenv
# source myenv/bin/activate
# pip install -r src/requirements.txt

4) Inside the web please do the necessary settings.
a) click on add a new web app.
b) next
c) select a python web framework 
- Manual configurations
- python 2.7
- next
d) Make these modification in - Code area.
Source code : /home/tuxfux/src
Working directory: /home/tuxfux   # leave it as default
wsgi configuraiton file: /var/www/tuxfux_pythonanywhere_com_wsgi.py  # leave it as default

e) virtualenv section.
i set my virtual env to /home/tuxfux/myenv

5) In the base console - run the collectstatic to get all the files.
# python manage.py collectstatic

6) Make changes to /home/tuxfux/src/mysite/settings/__init__.py

comment out the prod settings
comment the dev settings

from .base import *

#try:
#	from .dev import *
#except:
#	pass

try:
 	from .prod import *
except:
 	pass


# python manage.py makemigartions
# python manage.py migrate

6) back to webapp setup:

under - Static files:
URL -> /static/
Directory -> /home/tuxfux/src/project_public

7) go to the location - /var/www/tuxfux_pythonanywhere_com_wsgi.py

- commented out the below lines - line 19,47,82,83,88,89

- uncommented the lines - 76,77,
74 # +++++++++++ DJANGO +++++++++++
 75 # To use your own django app use code like this:
 76 import os
 77 import sys
 78 #
 79 ## assuming your django settings file is at '/home/tuxfux/mysite/mysite/settings.py'
 80 ## and your manage.py is is at '/home/tuxfux/mysite/manage.py'
 81 path = '/home/tuxfux/mysite'
 82 if path not in sys.path:
 83     sys.path.append(path)
 84 #
 85 os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'
 86 #
 87 ## then, for django >=1.5:
 88 from django.core.wsgi import get_wsgi_application
 89 application = get_wsgi_application()
 90 ## or, for older django <=1.4
 91 #import django.core.handlers.wsgi
 92 #application = django.core.handlers.wsgi.WSGIHandler()  

8) under web - click on Reload tuxfux.pythonanywhere.com
9) test your website under - http://tuxfux.pythonanywhere.com

10) run the runserver scripts to migrate the data.


