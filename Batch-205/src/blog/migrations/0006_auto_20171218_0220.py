# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20171216_0321'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='restaurant',
            name='place',
        ),
        migrations.RemoveField(
            model_name='waiter',
            name='restaurant',
        ),
        migrations.DeleteModel(
            name='Place',
        ),
        migrations.DeleteModel(
            name='Restaurant',
        ),
        migrations.DeleteModel(
            name='Waiter',
        ),
    ]
